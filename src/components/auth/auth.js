import axios from "axios";

export default {
  user: { roles: [], username: "", authenticated: false },
  login: function (context, username, password, redirect) {
    let token = btoa(username+":"+password);

    axios.get("https://builditnative.herokuapp.com/api/authenticate", {headers: {'Authorization': `Basic ${token}`}})
      .then(response => {
        this.username = username;
        console.log(username)
        this.user.roles = response.data;
        console.log("32321"+ this.user.roles)
        this.user.authenticated = true;
        console.log("before", window.localStorage)
        window.localStorage.setItem('token-'+this.username, token);
        console.log("after", window.localStorage.length);
        if (redirect)
          context.$router.push({path: redirect});
      })
      .catch(error => {
        console.log(error);
      });
  },
  hasAnyOf: function(roles) {
    return this.user.roles.find(role => roles.includes(role));
  },
  logout: function() {
    window.localStorage.removeItem('token-'+this.username);
    this.user = { roles: [], username: "", authenticated: false };
  },
  authenticated: function() {
    return this.user.authenticated;
  },
  getAuthHeader: function() {
    return {
    'Authorization': window.localStorage.getItem('token-'+this.username)
    }
  }
}
